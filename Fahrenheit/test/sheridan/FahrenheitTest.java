package sheridan;

/***
 * 
 * @author Roman Krutikov
 * 
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		int tempActual = Fahrenheit.fromCelsius(100);
		int tempExpected = 212;
		assertTrue("Conversion from Celsius to Fahrenheit failed", tempActual == tempExpected);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testFromCelsiusException() {
		int tempActual = Fahrenheit.fromCelsius(-100);
		fail("Conversion from Celsius to Fahrenheit failed");
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		int tempActual = Fahrenheit.fromCelsius(0);
		int tempExpected = 32;
		assertTrue("Conversion from Celsius to Fahrenheit failed", tempActual == tempExpected);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testFromCelsiusBoundaryOut() {
		int tempActual = Fahrenheit.fromCelsius(-1);
		fail("Conversion from Celsius to Fahrenheit failed");
	}

}
