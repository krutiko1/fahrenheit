package sheridan;

/***
 * 
 * @author Roman Krutikov
 * 
 */

public class Fahrenheit {

	public static int fromCelsius (int argument) throws IllegalArgumentException{
		if (argument < 0) {
			throw new IllegalArgumentException("This function does not accept negative values");
		}
		double temperture = (argument * 9.0/5.0)+32.0;
		return Math.round((float)temperture);
	}
	
}
